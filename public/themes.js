let theme = document.getElementById("theme")

class Mode {
    constructor(name, vars, colorScheme, prev) {
        this.name = name
        this.vars = vars
        this.colorScheme = colorScheme
        this.prev = prev
    }

    setVisibility(visible) {
        let elt = document.getElementById(this.name)

        if (elt) {
            elt.style.opacity = visible ? "100%" : "0%"
        } else {
            window.addEventListener(
                "load",
                () =>
                    (document.getElementById(this.name).style.opacity = visible
                        ? "100%"
                        : "0%"),
            )
        }
    }
}

let rootStyle = document.documentElement.style
let rootComputed = getComputedStyle(document.documentElement)

let pv = value => rootComputed.getPropertyValue(value)

let varNames = [
    "--background",
    "--foreground",
    "--accent",
    "--code-fg",
    "--blue",
    "--gray",
    "--green",
    "--magenta",
    "--orange",
    "--red",
    "--yellow",
    "--chip-green-bk",
    "--chip-green-bd",
    "--chip-red-bk",
    "--chip-red-bd",
    "--chip-blue-bk",
    "--chip-blue-bd",
    "--chip-yellow-bk",
    "--chip-yellow-bd",
    "--chip-purple-bk",
    "--chip-purple-bd",
]

let primaryVars = varNames.map(name => [name, pv(name)]).filter(v => v[1] != "")

let hcVars = varNames.map(name => [name, pv(name + "-hc")]).filter(v => v[1] != "")

let modes = [
    new Mode("dark-mode", primaryVars, "only dark", "light-mode"),
    new Mode("light-mode", primaryVars, "only light", "high-contrast"),
    new Mode("high-contrast", hcVars, "only dark", "dark-mode"),
]

let lightModeQuery = window.matchMedia("prefers-color-scheme: light")
let contrastQuery = window.matchMedia("prefers-contrast: more")

let currentMode = localStorage.getItem("theme")

function modeFromQuery(lightMode, highContrast) {
    return lightMode
        ? "light-mode"
        : highContrast
        ? "high-contrast"
        : "dark-mode"
}

let lightMode = lightModeQuery.matches
let highContrast = contrastQuery.matches

if (!currentMode) {
    currentMode = modeFromQuery(lightMode, highContrast)
}

function setMode(mode) {
    currentMode = mode
    localStorage.setItem("theme", currentMode)
    for (const option of modes) {
        option.setVisibility(option.prev == mode)

        if (option.name != mode) {
            continue
        }

        for (const [name, value] of option.vars) {
            rootStyle.setProperty(name, value)
        }

        rootStyle.setProperty("color-scheme", option.colorScheme)
    }
}

function toggleMode() {
    setMode(modes.find(mode => mode.prev == currentMode)?.name ?? "dark-mode")
}

setMode(currentMode)

lightModeQuery.addEventListener("change", e => {
    lightMode = e.matches
    setMode(modeFromQuery(lightMode, highContrast))
})

contrastQuery.addEventListener("change", e => {
    highContrast = e.matches
    setMode(modeFromQuery(lightMode, highContrast))
})

// Don't allow transitions to take effect until after rendering (to prevent flashes)
requestAnimationFrame(() =>
    requestAnimationFrame(() => {
        document.styleSheets.item(0).insertRule(`
        * {
            transition-duration: 200ms;
        }
    `)
    }),
)
