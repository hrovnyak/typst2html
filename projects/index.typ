/*---
title: "Projects",
description: "Some of my interesting projects",
keywords: [],
---*/

// Use `align` to leak any kind of stylable information out of pandoc
// This is a HUGE hack, but too bad
// Hopefully pandoc gets better typst support someday
#let maintaining = align(left, box("Maintaining"))
#let cancelled = align(right, box("Cancelled"))
#let finished = align(center, box("Finished"))
#let on-pause = align(top, box("On pause"))
#let in-progress = align(bottom, box("In progress"))

= My Projects

Here are some of the cool things I've been working on:

== Qter #in-progress

Turning a rubik's cube into a computer using group theory. I'm currently working on the compiler from our custom programming language to rubik's cube moves.

#link("https://arhan.sh/projects/")[My teammate] \
#link("https://github.com/ArhanChaudhary/qter")[Github]

== My Website #maintaining
You're here right now! A static site compiled by a Rust program around Pandoc. It contains whatever I feel like having on the interwebs. 

#link("https://gitlab.com/hrovnyak/hrovnyak")[Gitlab]

== Nebulous #on-pause
A desktop application written in Rust and Svelte using Tauri. It allows you to keep an encrypted diary along with dates and categorization. I already use this every day!

Next steps: I'm rewriting the program in 100% Rust using #link("https://github.com/iced-rs/iced")[Iced]. It's also missing features that are important to me like embedding images and a better tag system.

#link("https://gitlab.com/nebulous-app/nebulous")[Gitlab]

== Light Encoder #on-pause

A homemade functional programming language designed for programming light shows for individually addressable programmable lights. It comes with a website designed to be hosted on a Raspberry Pi that will allow lights plugged into its GPIO to be remotely controlled.

Next steps: Actually try it out and make something! I'm sure doing so would reveal any hidden horrible flaws I need to fix.

#link("https://gitlab.com/tycha/light-programmer")[Language implementation] \
#link("https://gitlab.com/tycha/lights-controller")[Website implementation]

== const-units #on-pause

An experimental Rust crate implementing compile time verification of unit consistency using the experimental `generic_const_exprs` and `adt_const_params` features.

Next steps: I want to make it possible to express custom non-SI units like "pixels" or "bytes".

#link("https://crates.io/crates/const-units")[crates.io]

== mmm... SOUP! #finished

A video game written in Godot about grabbing letters out of soup to spell words. The game was made alongside two of my friends for the 2022-23 FBLA Computer Game and Simulation Programming event and we got 7th place (out of 15) in Pennsylvania!

#link("https://github.com/Element21/FBLA-Computer-Game-Simulation-Programming-22")[Github]

== Minecraft Wasm Runtime #cancelled
A minecraft mod whose end goal was adding WebAssembly plugin support to Minecraft as an alternative to #link("https://github.com/gnembon/fabric-carpet/wiki/Scarpet")[Scarpet].

Originally it was implemented as a homemade WebAssembly bytecode interpreter in Java, but I threw out the code after learning about JNI and Wasmtime. This is the project where I first learned the Rust programming language. I stopped the project after realizing how much effort it would take to bring it to fully featured.

#link("https://github.com/Xendergo/Minecraft-wasm-runtime")[Github]

== Botter #finished
A discord bot written in C\# for my friends. It implements sniping (undeleting messages), querying images from google, and numerous RPG features, including battles, status effects, weapons, money, and in-game stock trading based on real life stock information.

#link("https://github.com/Xendergo/Botter-rewrite")[Github]

== School counter #finished
A silly website that would generate a countdown to the end of school as well as math art based on #link("https://www.youtube.com/watch?v=qhbuKbxJsk8")[times tables] from the value of the countdown.

#link("https://xendergo.github.io/School-counter/")[Link] \
#link("https://github.com/Xendergo/School-counter")[Github]

== Laser mod #finished
A Minecraft mod adding lasers to Minecraft. The lasers have wavelength and strength parameters, each having different effects. The mod has laser emitters, detectors, mirrors, beam splitters, and fiber optics.

#link("https://github.com/Xendergo/laser-mod")[Github]

== The depths of my Github #finished
When I started getting serious about programming, I made lots of little projects in raw Javascript and HTML. None of them are worth noting individually, so I'll pick out a few of my favorites here:

=== Vogueify
Edits a user-uploaded picture onto the cover of Vogue along with some effects implemented using image buffers. This was a trend going around at the time.

#link("https://xendergo.github.io/Vogueify/")[Link] \
#link("https://github.com/Xendergo/Vogueify")[Github]

=== Markov Chain

A random word generator using a Markov chain generated from a corpus. It supports arbitrary UTF-8 characters (like International Phonetic Alphabet), unlike any alternative I could find on the internet at the time. It's super useful for generated randomized names.

#link("https://xendergo.github.io/Markov-chain/")[Link] \
#link("https://github.com/Xendergo/Markov-chain")[Github]

=== Dot survival
A simple canvas based game where you shoot at squares. One of my first finished projects (that I have records of)!

#link("https://thriller-film-productions.github.io/Dot-Survival")[Link] \
#link("https://github.com/Thriller-Film-Productions/Dot-Survival")[Github]
