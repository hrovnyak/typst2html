use color_eyre::{
    eyre::{eyre, Context},
    Result, Section,
};
use quick_xml::{
    events::{BytesEnd, BytesStart, Event},
    Reader, Writer,
};
use serde::Deserialize;
use std::{
    fs::{self, File},
    io::{BufReader, Read},
    path::{Path, PathBuf},
    process::{Command, Stdio},
    sync::OnceLock,
};

use crate::postprocessing::target_blank;

static TOPBAR: OnceLock<String> = OnceLock::new();

pub fn compile<'a>(
    file: &Path,
    template: &Path,
    vars: impl IntoIterator<Item = (&'a str, String)>,
) -> Result<(Elements, Meta)> {
    let path = match file.file_stem().and_then(|v| v.to_str()) {
        Some("index") => file
            .parent()
            .expect("there to be a parent")
            .to_str()
            .expect("paths to be UTF8")
            .to_owned(),
        _ => file
            .with_extension("")
            .to_str()
            .expect("paths to be UTF8")
            .to_owned(),
    };

    let file_in_public = Path::new("public").canonicalize()?.join(file);

    let file = file.canonicalize()?;

    let out = Path::new("public").join(match file_in_public.file_stem().map(|v| v.to_str()) {
        Some(Some("index")) => file_in_public.with_extension("html"),
        _ => file_in_public.with_extension("").join("index.html"),
    });

    let meta = Meta::extract_from(&file, format!("/{}", path), out.to_owned())
        .wrap_err_with(|| eyre!("pulling meta from {file:?}"))?;

    let topbar = TOPBAR.get_or_try_init(|| fs::read_to_string("data/topbar.html"))?;

    // `rev` is only supported for double ended iterator
    for parent in out
        .ancestors()
        .skip(1)
        .collect::<Vec<_>>()
        .into_iter()
        .rev()
    {
        if !parent.exists() {
            fs::create_dir(parent)?;
        }
    }

    let mut child = Command::new("pandoc")
        .arg(&file)
        .arg("--standalone")
        .arg("--katex")
        .arg("--template")
        .arg(template)
        .arg("-M")
        .arg("document-css=false")
        // Disable default highlighting styles
        .arg("--variable=highlighting-css=")
        .args(
            vars.into_iter()
                .map(|v| format!("--variable={}:{}", v.0, v.1)),
        )
        .args(meta.to_pandoc_args())
        .arg(format!("--variable=topbar:{topbar}"))
        .stdout(Stdio::piped())
        .spawn()
        .with_note(|| format!("While compiling {file:?}"))?;

    let stdout = child.stdout.take().expect("stdout to exist");

    let mut elements = Elements::read(stdout)?;

    target_blank(&mut elements, false)?;

    Ok((elements, meta))
}

#[derive(Deserialize)]
pub struct Meta {
    pub title: String,
    pub description: String,
    pub keywords: Vec<String>,
    #[serde(skip)]
    pub path: String,
    #[serde(skip)]
    pub output: PathBuf,
}

impl Meta {
    fn to_pandoc_args(&self) -> impl Iterator<Item = String> + '_ {
        // Give a compile error if I add new fields and forget to use them
        let Meta {
            title,
            description,
            keywords,
            path,
            output: _,
        } = self;

        ["--metadata".to_owned(), format!("title={title}")]
            .into_iter()
            .chain([
                format!("--variable=path:{path}"),
                format!("--variable=description:{description}"),
            ])
            .chain(
                keywords
                    .iter()
                    .map(|keyword| format!("--variable=keywords:{keyword}")),
            )
    }

    fn extract_from(file: &Path, path: String, output: PathBuf) -> Result<Meta> {
        let str = fs::read_to_string(file)?;

        let text = match file.extension().map(|v| v.as_encoded_bytes()) {
            Some(b"typ") => {
                let a = match str.find("/*---") {
                    Some(n) => n + 5,
                    None => return Err(eyre!("Document contains no metadata section")),
                };
                let b = match str.find("---*/") {
                    Some(n) => n,
                    None => return Err(eyre!("Meta section is unclosed")),
                };

                &str[a..b]
            }
            ext => return Err(eyre!("Unknown how to pull metadata from a {ext:?} file")),
        };

        let mut meta: Meta = ron::de::from_str(&format!("Meta({text})"))?;

        meta.path = path;
        meta.output = output;

        Ok(meta)
    }
}

#[derive(Clone)]
pub struct Elements(pub Vec<Element>);

#[derive(Clone)]
pub enum Element {
    Event(Event<'static>),
    Tag {
        start: BytesStart<'static>,
        end: BytesEnd<'static>,
        children: Elements,
    },
}

impl Elements {
    pub fn read(stdout: impl Read) -> Result<Elements> {
        let mut reader = Reader::from_reader(BufReader::new(stdout));
        let mut base: Vec<Element> = vec![];
        let mut levels: Vec<(BytesStart<'static>, Vec<Element>)> = vec![];
        let mut buf = vec![];

        loop {
            match reader.read_event_into(&mut buf)?.into_owned() {
                Event::Eof => break,
                Event::Start(start) => levels.push((start, vec![])),
                Event::End(end) => {
                    let Some((start, children)) = levels.pop() else {
                        return Err(eyre!("Too many closing tags"));
                    };

                    match levels.last_mut() {
                        Some(last) => last.1.push(Element::Tag {
                            start,
                            end,
                            children: Elements(children),
                        }),
                        None => base.push(Element::Tag {
                            start,
                            end,
                            children: Elements(children),
                        }),
                    }
                }
                event => match levels.last_mut() {
                    Some(last) => last.1.push(Element::Event(event)),
                    None => base.push(Element::Event(event)),
                },
            }
        }

        Ok(Elements(base))
    }

    pub fn write(self, out: &Path) -> Result<()> {
        let mut writer = Writer::new(File::create(out)?);

        self.write_impl(&mut writer)
    }

    fn write_impl(self, writer: &mut Writer<File>) -> Result<()> {
        for element in self.0 {
            element.write(writer)?;
        }

        Ok(())
    }
}

impl Element {
    fn write(self, writer: &mut Writer<File>) -> Result<()> {
        match self {
            Element::Event(e) => writer.write_event(e)?,
            Element::Tag {
                start,
                end,
                children,
            } => {
                writer.write_event(Event::Start(start))?;
                children.write_impl(writer)?;
                writer.write_event(Event::End(end))?;
            }
        }

        Ok(())
    }
}
