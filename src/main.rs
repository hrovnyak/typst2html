#![feature(once_cell_try)]

use std::{collections::HashMap, fs, io::Cursor, path::Path, sync::LazyLock, thread};

use crate::history::get_history;
use color_eyre::Result;
use history::History;
use postprocessing::{add_heading_links, replace_labels};
use quick_xml::events::{BytesEnd, BytesStart, BytesText, Event};

mod compile;
mod history;
mod postprocessing;

use compile::*;

fn main() -> Result<()> {
    color_eyre::install()?;

    let a = thread::spawn(|| compile_posts("posts"));

    let b = thread::spawn(|| {
        compile(
            Path::new("projects/index.typ"),
            Path::new("data/index-template.html"),
            [("css", "/projects/style.css".to_owned())],
        )
        .and_then(|(mut elements, meta)| {
            add_heading_links(&mut elements)?;
            elements.write(&meta.output)
        })
    });

    let c = thread::spawn(|| {
        compile_index(
            Path::new("index.typ"),
            [("/posts", "Posts"), ("/projects", "Projects")],
        )
    });

    a.join().unwrap()?;
    b.join().unwrap()?;
    c.join().unwrap()?;

    Ok(())
}

fn compile_posts(name: &str) -> Result<()> {
    let mut posts = vec![];

    let mut threads = vec![];

    for maybe_post in fs::read_dir(name)? {
        let entry = maybe_post?;

        const EXTS: &[&str] = &["md", "typ"];

        if EXTS
            .iter()
            .map(|v| Some(v.as_ref()))
            .all(|v| v != entry.path().extension())
            || entry.path().file_stem() == Some("index".as_ref())
        {
            continue;
        }

        let history = get_history(&entry.path())?;

        threads.push((
            entry.path(),
            history.to_owned(),
            thread::spawn(move || compile_post(&entry.path(), &history)),
        ));
    }

    for (path, history, thread) in threads {
        let meta = thread.join().unwrap()?;

        posts.push((path, history, meta));
    }

    posts.sort_by_key(|v| v.1.changes.last().unwrap().time);
    posts.reverse();

    compile_index(
        Path::new(&format!("{name}/index.typ")),
        posts.iter().map(|v| (&*v.2.path, &*v.2.title)),
    )?;

    Ok(())
}

static WEBRING: LazyLock<Element> = LazyLock::new(|| {
    Elements::read(Cursor::new(include_str!("../data/webring.html")))
        .unwrap()
        .0
        .first()
        .unwrap()
        .to_owned()
});

fn compile_index<'a>(
    file: &Path,
    index: impl IntoIterator<Item = (&'a str, &'a str)>,
) -> Result<Meta> {
    let index = Element::Tag {
        start: BytesStart::new("ul"),
        end: BytesEnd::new("ul"),
        children: Elements(
            index
                .into_iter()
                .map(|(path, inner)| Element::Tag {
                    start: BytesStart::new("li"),
                    end: BytesEnd::new("li"),
                    children: Elements(vec![Element::Tag {
                        start: BytesStart::new("a").with_attributes([("href", path)]),
                        end: BytesEnd::new("a"),
                        children: Elements(vec![Element::Event(Event::Text(
                            BytesText::new(inner).into_owned(),
                        ))]),
                    }]),
                })
                .collect(),
        ),
    };

    let (mut elements, meta) = compile(file, Path::new("data/index-template.html"), [])?;

    let mut map = HashMap::new();

    map.insert("index".to_owned(), index);
    map.insert("webring".to_owned(), WEBRING.to_owned());

    replace_labels(&mut elements, &map)?;

    elements.write(&meta.output)?;

    Ok(meta)
}

fn compile_post(file: &Path, history: &History) -> Result<Meta> {
    let latest = history.changes.first();

    let (mut elements, meta) = compile(
        file,
        Path::new("data/post-template.html"),
        latest
            .map(|change| ("date", change.time.format("%b %-d, %Y").to_string()))
            .into_iter()
            .chain(history.changes.iter().map(|change| {
                (
                    "history",
                    format!(
                        "<a{}>{}</a>",
                        change
                            .revision
                            .map(|rev| format!(
                                " href=\"https://gitlab.com/hrovnyak/hrovnyak.gitlab.io/-/blob/{rev}/{}\" target=\"_blank\"",
                                file.to_str().expect("paths to be utf8")
                            ))
                            .unwrap_or_default(),
                        change.time.format("%b %-d %Y")
                    ),
                )
            })),
    )?;

    add_heading_links(&mut elements)?;

    elements.write(&meta.output)?;

    Ok(meta)
}
