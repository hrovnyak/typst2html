use chrono_tz::Tz;
use color_eyre::Result;
use itertools::Itertools;
use std::{
    path::Path,
    sync::{Mutex, OnceLock},
};

use chrono::{DateTime, Utc};
use git2::{Commit, Delta, DiffOptions, Oid, Repository};

const TIMEZONE: Tz = chrono_tz::EST;

#[derive(Clone, Debug)]
pub struct History {
    pub changes: Vec<Change>,
}

#[derive(Debug, Clone)]
pub struct Change {
    pub time: chrono::DateTime<Tz>,
    pub revision: Option<Oid>,
}

static REPOSITORY: OnceLock<Mutex<Repository>> = OnceLock::new();

pub fn get_history(file: &Path) -> Result<History> {
    let repo = REPOSITORY
        .get_or_try_init(|| Repository::open(".").map(Mutex::new))?
        .lock()
        .unwrap();

    let bad_deltas = [Delta::Unmodified, Delta::Unreadable, Delta::Ignored];

    let mut revs = repo.revwalk()?;
    revs.push_head()?;
    revs.simplify_first_parent()?;

    let mut changes = vec![];

    let mut process_diff = |diff: git2::Diff<'_>, commit: Option<&Commit>| {
        for delta in diff.deltas() {
            if delta.new_file().path() == Some(file) && !bad_deltas.contains(&delta.status()) {
                changes.push(Change {
                    time: match commit {
                        None => Utc::now().with_timezone(&TIMEZONE),
                        Some(commit) => DateTime::from_timestamp(commit.time().seconds(), 0)
                            .expect("the timestamp to be valid")
                            .with_timezone(&TIMEZONE),
                    },
                    revision: commit.map(|v| v.id()),
                });
                break;
            }
        }
    };

    let mut prev: Option<Commit> = None;

    for rev in revs.collect_vec().into_iter().rev() {
        let rev = rev?;
        let commit = repo.find_commit(rev)?;

        let mut options = DiffOptions::new();
        let options = options.include_untracked(true).recurse_untracked_dirs(true);
        let diff = repo.diff_tree_to_tree(
            prev.map(|v| v.tree()).transpose()?.as_ref(),
            Some(&commit.tree()?),
            Some(options),
        )?;

        process_diff(diff, Some(&commit));

        prev = Some(commit);
    }

    let mut options = DiffOptions::new();
    let mut options = options.include_untracked(true).recurse_untracked_dirs(true);
    let diff =
        repo.diff_tree_to_workdir(Some(&repo.head()?.peel_to_tree()?), Some(&mut options))?;

    process_diff(diff, None);

    changes.sort_unstable_by_key(|change| change.time);
    changes.reverse();

    Ok(History { changes })
}
