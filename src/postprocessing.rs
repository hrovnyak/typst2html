use std::{
    collections::{HashMap, HashSet},
    mem,
    str::from_utf8,
};

use crate::{Element, Elements};
use color_eyre::Result;
use itertools::Itertools;
use quick_xml::events::{BytesEnd, BytesStart, BytesText, Event};

pub fn target_blank(elements: &mut Elements, found_body: bool) -> Result<()> {
    for element in &mut elements.0 {
        if let Element::Tag {
            start,
            end: _,
            children,
        } = element
        {
            if found_body && start.name().into_inner() == b"a" {
                start.push_attribute(("target", "_blank"));
            }

            let mut this_is_body = found_body;

            if !this_is_body {
                this_is_body = start
                    .try_get_attribute("id")?
                    .is_some_and(|id| &*id.value == b"body");
            }

            target_blank(children, this_is_body)?;
        }
    }

    Ok(())
}

pub fn replace_labels(elements: &mut Elements, labels: &HashMap<String, Element>) -> Result<()> {
    'next_element: for element in &mut elements.0 {
        if let Element::Tag {
            start: _,
            end: _,
            children,
        } = element
        {
            for child in &children.0 {
                if let Element::Tag {
                    start,
                    end: _,
                    children: _,
                } = child
                {
                    if let Some(id) = start
                        .try_get_attribute("id")?
                        .as_ref()
                        .and_then(|v| from_utf8(v.value.as_ref()).ok())
                    {
                        if let Some(to_replace) = labels.get(id) {
                            *element = to_replace.to_owned();
                            continue 'next_element;
                        }
                    }
                }
            }

            replace_labels(children, &labels)?;
        }
    }

    Ok(())
}

pub fn add_heading_links(elements: &mut Elements) -> Result<()> {
    let mut used_ids = HashSet::new();

    fn texts(elements: &Elements) -> Result<Vec<String>> {
        let mut out = vec![];

        for element in &elements.0 {
            match element {
                Element::Event(Event::Text(text)) => out.push(String::from_utf8(text.to_vec())?),
                Element::Tag {
                    start: _,
                    end: _,
                    children,
                } => out.extend_from_slice(&texts(children)?),
                _ => {}
            }
        }

        Ok(out)
    }

    fn add_heading_links_rec(
        elements: &mut Elements,
        used_ids: &mut HashSet<String>,
    ) -> Result<()> {
        for element in elements.0.iter_mut() {
            if let Element::Tag {
                start,
                end: _,
                children,
            } = element
            {
                let name = start.name().into_inner();

                let headings: [&[u8]; 6] = [b"h1", b"h2", b"h3", b"h4", b"h5", b"h6"];

                let class_attr = start.try_get_attribute("class")?;

                if headings.contains(&name)
                    && class_attr
                        .as_ref()
                        .map(|attr| attr.value.split(|v| *v == b' ').any(|v| &v == b"title"))
                        != Some(true)
                {
                    let id_prefix = texts(children)?
                        .iter()
                        .flat_map(|v| v.split(' '))
                        .take(4)
                        .join("-");
                    let mut disambiguator: Option<u64> = None;
                    let id = loop {
                        let potential_id = url_escape::encode_fragment(&format!(
                            "{id_prefix}{}",
                            match disambiguator {
                                Some(n) => n.to_string(),
                                None => String::new(),
                            }
                        ))
                        .into_owned();

                        if !used_ids.contains(&potential_id) {
                            used_ids.insert(potential_id.to_owned());
                            break potential_id;
                        } else {
                            disambiguator = Some(match disambiguator {
                                Some(n) => n + 1,
                                None => 2,
                            });
                        }
                    };

                    start.push_attribute(("id", &*id));

                    let prev_children = mem::take(&mut children.0);

                    children.0.push(Element::Tag {
                        start: BytesStart::new("span"),
                        end: BytesEnd::new("span"),
                        children: Elements(prev_children),
                    });

                    children.0.push(Element::Tag {
                        start: BytesStart::new("a").with_attributes([
                            ("href", &*format!("#{id}")),
                            ("class", "hash-link"),
                        ]),
                        end: BytesEnd::new("a"),
                        children: Elements(vec![Element::Event(Event::Text(BytesText::new("#")))]),
                    });
                }

                add_heading_links_rec(children, used_ids)?;
            }
        }

        Ok(())
    }

    add_heading_links_rec(elements, &mut used_ids)
}
