/*---
title: "My favorite books",
description: "A list of all the books I've enjoyed reading; updated incrementally",
keywords: ["books", "reading list", "book recommendations"],
---*/

#let book(author, ..names) = {
    let names = names.pos()
    
    [#names.map(v => [_#v;_]).intersperse(", ").join(); - #author;]
    // [#names; - #author;]
}

This is my list of books that I really liked and would recommend to others. I plan on updating this list as I read more. The list is in the order that I happened to read the books in. Why don't you check out the list! Don't worry, I won't spoil anything.

- #book([Philip Pullman], [His Dark Materials])
    - This is a fun book series with fantastic fantasy worldbuilding that makes you feel wonder and excitement as you read through it. I wish there was more I could praise without giving major spoilers. If you enjoy fantasy/sci-fi, this series is absolutely worth your time.  

- #book([Líu Cíxīn], [Remembrance of Earth's Past (The Three Body Problem)])
    - The series is exciting, mysterious, and made me more thankful for the planet we live on. Líu is extremely clever and draws on lots of cool ideas from real science. I would highly recommend this book series to anyone.

- #book([Cliff Stoll], [The Cuckoo's Egg])
    - A true story about how the author tracked down a hacker in his computer system through the 1980s era internet. Takes place in the era of Vax/VMS, dial up internet, Berkeley vs. AT&T Unix, Tymnet, and people using the word "bailiwick". I found it interesting how he complains about the inaction of government while contrasting it with its value as an institution.

- #book([Arthur C. Clarke], [2001: A Space Odyssey])
    - Honestly, the movie wasn't that great; the book actually makes sense and conveys the plot effectively. I really enjoyed it and would highly recommend it, especially if you didn't understand the movie.

- #book([William Spaniel], [What Caused the Russia-Ukraine War], [How Ukraine Survived])
    - The answer isn't what you think... These books will change the way you see international politics (or you can just watch his #link("https://youtube.com/@Gametheory101")[youtube channel]). As it happens, "because Russia wanted \_\_\_" can't be a complete answer because it fails to explain why both sides choose to incur the cost of war over negotiating peacefully.

- #book([Devon Price], [Unmasking Autism])
    - When I was diagnosed as autistic I had no idea what that meant. The diagnostic criteria simply aren't helpful. This book shows what it _actually means_ to be autistic, and how to square that with your conception of yourself. I would _highly_ recommend this to anyone newly diagnosed or considering whether or not they could be autistic. 

- #book([Travis Jeppesen], [See You Again in Pyongyang])
    - Ever wondered what North Korea is _really_ like? Live vicariously through this guy! This book will give you insight into North Korea and how their country and culture function. The western propaganda we're exposed to isn't terribly honest.

- #book([Andy Weir], [Project Hail Mary])
    - This is a super engaging sci-fi book that justifies everything it can using real science instead of just handwaving everything as #emoji.rainbow _technology!_. I really loved this book; I couldn't stop thinking about it for an embarrasing amount of time.

- #book([Madeleine L'Engle], [A Wrinkle in Time], [A Wind in the Door], [A Swiftly Tilting Planet])
    - These three were the only ones from the series that I happened to have. A lighthearted fantasy series about kids going on bizarre adventures and saving the universe at least once.

- #book([Timothy Zahn], [Star Wars: Thrawn], [Thrawn: Alliances], [Thrawn: Treason])
    - imagine if... star wars was actually good? If you like Star Wars but hate the direction it's going, try reading these!

- #book([George Orwell], [1984])
    - A fun romcom where the couple tries to get together against all odds, and they succeed! -- at least for a little while... /j

- #book([Dianne Salerni], [The Eighth Day], [The Inquisitors Mark], [The Morrigan's Curse])
    - An exciting and engaging series that blends fantasy with modern reality in a way I haven't seen before.

- #book([Kathryn Lasky], [Guardians of Ga'Hoole (the whole series)])
    - I loved this entire series as a fifth grader. I don't even remember what most of it was about; something with owls, slavery, brainwashing, a funny tree, and fire fortunetelling. Maybe I should reread them...
